package org.mybatis.jpetstore.init;

import org.mybatis.jpetstore.domain.Account;
import org.mybatis.jpetstore.mapper.AccountMapper;

public class InitAccount {

    public static void init(AccountMapper accountMapper){
        Account account = new Account();
        account.setUsername("root");
        account.setPassword("123456");
        account.setEmail("root@email.com");
        account.setFirstName("jpetstore");
        account.setLastName("mybatis");
        account.setAddress1("TCSE");
        account.setAddress2("ISCAS");
        account.setCity("HD");
        account.setState("BJ");
        account.setCountry("CNY");
        account.setZip("100000");
        account.setPhone("13656728987");
        accountMapper.insertAccount(account);
    }

}
